import React from 'react'
import { StyleSheet, FlatList, Text, View, Image, ActivityIndicator } from 'react-native'

export default class FlatListV extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            page: 1,
        }
    }

    renderRow = ({ item }) => {
        return (
            <View style={styles.item}>
                <Image source={{ uri: item.url }} style={styles.itemImage} />
                <Text style={styles.itemText}>
                    {item.title}
                </Text>
                <Text style={styles.itemText}>
                    {item.id}
                </Text>
            </View>

        )
    }

    componentDidMount() {
            this.getData()
    }
    handleMore = () => {
        this.setState({
            page: this.state.page + 1
        }, this.getData)
    }

    renderFooter = () => {
        return (
            <View styles={styles.loader}>
                <ActivityIndicator size="large" />
            </View>
        )
    }

    getData = async () => {
        const url = `https://jsonplaceholder.typicode.com/photos?_limit=10&_page=${this.state.page}`
        fetch(url).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    data: this.state.data.concat(responseJson)
                })
            })
    }

    render() {
        console.log("state", this.state)
        return (
            <FlatList
                style={styles.container}
                data={this.state.data}
                renderItem={this.renderRow}
                keyExtractor={(item, index) => index.toString()}
                onEndReached={this.handleMore}
                onEndReachedThreshold={0.5}
                // ListFooterComponent={this.renderFooter}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        backgroundColor: "#F5FCFF"
    },
    item: {
        borderBottomColor: '#ccc',
        marginBottom: 10,
        borderBottomWidth: 1
    },
    itemImage: {
        width: '100%',
        height: 200,
        resizeMode: 'cover'
    },
    itemText: {
        fontSize: 16,
        padding: 5
    },
    loader: {
        marginTop: 10,
        alignItems: 'center'
    }
})